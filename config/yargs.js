const descripcion = {
    demand: true,
    alias: 'd',
    desc: 'Descripcion de la tarea por hacer'
}

const compledato = {
    default: true,
    alais: 'c',
    desc: 'Marca como completada o pendiente la tarea'
}

const argv = require('yargs')
    .command('crear', 'Crea un elemento por Hacer', {

    })
    .command('actualizar', 'Actualiza el estado completado de una tarea', {
        descripcion: {
            demand: true,
            alias: 'd',
            desc: 'Descripcion de la tarea por hacer'
        },
        compledato: {
            default: true,
            alais: 'c',
            desc: 'Marca como completada o pendiente la tarea'
        }
    })
    .command('borrar', 'elimina una tarea de la DB', {
        descripcion: {
            desc: 'borra un elemnto',
            alias: 'd',
            demand: true,
        }
    })
    .help()
    .argv;

module.exports = {
    argv
}